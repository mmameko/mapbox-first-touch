import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';
import {MbRouteInterface} from '../types/mbRoute.interface';
import { map } from 'rxjs/internal/operators/map';
import {MapDrivingModeEnum} from '../enums/mapDrivingMode.enum';
import {MapPathInterface} from '../types/mapPath.interface';
import {MapPathColorEnum} from '../enums/mapPathColor.enum';
import {MapRouteInterface} from '../types/mapRoute.interface';
import {PointInterface} from 'src/app/shared/types/point.interface';
import {RandomizeService} from 'src/app/shared/modules/randomize/services/randomize.service';
import { MbResponseInterface } from '../types/mbResponse.interface';

@Injectable()
export class MbRoutesService {
  constructor(private httpClient: HttpClient, private randomizeService: RandomizeService) {
  }

  getRoutes(points: PointInterface[], drivingMode: MapDrivingModeEnum): Observable<MapRouteInterface[]> {
    const url = `${environment.mapApiUrl}${drivingMode}/${this.encodeUrlPoints(points)}`;
    const params = {
      geometries: 'geojson',
      access_token: environment.mapToken,
      alternatives: 'true'
    };

    return this.httpClient
      .get<MbResponseInterface>(url, {params})
      .pipe(
        map(
          (response: MbResponseInterface) => {
            const routes: MapRouteInterface[] = [];

            response.routes.forEach((route) => {
              const paths: MapPathInterface[] = [];
              let startId = 0;
              let totalPathAmount = route.geometry.coordinates.length - 1;

              while (totalPathAmount > 0) {
                const pathLength = this.randomizeService.randomIntFromRange(1, totalPathAmount);

                paths.push({
                  weight: this.randomizeService.randomFromEnum(MapPathColorEnum),
                  geometry: {
                    type: route.geometry.type,
                    coordinates: route.geometry.coordinates.slice(startId, startId + pathLength + 1)
                  }
                });

                totalPathAmount -= pathLength;
                startId += pathLength;
              }

              routes.push({
                ...route,
                paths
              });
            });

            return routes;
          }
        )
      );
  }

  private encodeUrlPoints(points: PointInterface[]): string {
    return encodeURIComponent(
      points.map(point => `${point.lng},${point.lat}`).join(';')
    );
  }
}
