import {ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MapDrivingModeEnum} from '../../enums/mapDrivingMode.enum';
import {select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {
  drivingModeSelector,
  isSidebarVisibleSelector,
  isSubmittingSelector,
  pointsSelector,
  routesSelector,
  selectedRouteClustersSelector,
  selectedRouteIdSelector,
  selectedRouteSelector,
  unSelectedRoutesSelector
} from '../../store/selectors';
import {
  addPoint,
  changeDrivingMode,
  deletePoint,
  resetModel,
  mapRoutesAction,
  selectRouteMode,
  toggleSidebarVisibility
} from '../../store/actions/mapRoutesAction';
import {MapMouseEvent} from 'mapbox-gl';
import {environment} from '../../../../environments/environment';
import {MapPathInterface} from '../../types/mapPath.interface';
import {MapRouteInterface} from '../../types/mapRoute.interface';
import {SelectItem} from 'primeng/api';
import {MapClusteredRouteInterface} from '../../types/mapClusteredRoute.interface';
import { PointInterface } from 'src/app/shared/types/point.interface';

@Component({
  selector: 'app-directions',
  templateUrl: './mapRoutes.component.html',
  styleUrls: ['./mapRoutes.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MapRoutesComponent implements OnInit, OnDestroy {
  drivingModes: SelectItem[] = [
    {
      value: MapDrivingModeEnum.DRIVING_TRAFFIC,
      title: 'Driving Traffic',
      icon: 'fas fa-traffic-light'
    },
    {
      value: MapDrivingModeEnum.DRIVING,
      title: 'Driving',
      icon: 'fas fa-car'
    },
    {
      value: MapDrivingModeEnum.WALKING,
      title: 'Walking',
      icon: 'fas fa-walking'
    },
    {
      value: MapDrivingModeEnum.CYCLING,
      title: 'Cycling',
      icon: 'fas fa-biking'
    }
  ];
  mapConfigs = environment.mapConfigs;

  isSubmitting$: Observable<boolean>;
  points$: Observable<PointInterface[]>;
  paths$: Observable<MapPathInterface[]>;
  selectedRouteId$: Observable<number>;
  unSelectedRoutes$: Observable<MapRouteInterface[] | null>;
  clusteredRoutes$: Observable<MapClusteredRouteInterface>;
  routes$: Observable<MapRouteInterface[] | null>;
  drivingMode$: Observable<string>;
  isSidebarVisible$: Observable<boolean>;

  constructor(private store: Store) {}

  ngOnInit(): void {
    this.initValues();
  }

  ngOnDestroy(): void {
    this.store.dispatch(resetModel({}));
  }

  initValues(): void {
    this.isSubmitting$ = this.store.pipe(
      select(isSubmittingSelector)
    );
    this.points$ = this.store.pipe(
      select(pointsSelector)
    );
    this.drivingMode$ = this.store.pipe(
      select(drivingModeSelector)
    );
    this.routes$ = this.store.pipe(
      select(routesSelector)
    );
    this.unSelectedRoutes$ = this.store.pipe(
      select(unSelectedRoutesSelector)
    );
    this.selectedRouteId$ = this.store.pipe(
      select(selectedRouteIdSelector)
    );
    this.isSidebarVisible$ = this.store.pipe(
      select(isSidebarVisibleSelector)
    );
    this.clusteredRoutes$ = this.store.pipe(
      select(selectedRouteClustersSelector)
    );
  }

  searchRoutes(): void {
    this.store.dispatch(mapRoutesAction({points: []}));
  }

  addPoint(e: MapMouseEvent): void {
    e.originalEvent.cancelBubble = true;

    this.store.dispatch(addPoint(e.lngLat));
    this.searchRoutes();
  }

  removePoint(id): void {
    this.store.dispatch(deletePoint({ id  }));
    this.searchRoutes();
  }

  drivingModeChanged({value}): void {
    this.store.dispatch(changeDrivingMode({ drivingMode: value }));
    this.searchRoutes();
  }

  selectRoute(selectedRouteId): void {
    this.store.dispatch(selectRouteMode({ selectedRouteId }));
  }

  toggleSidebarVisibility(): void {
    this.store.dispatch(toggleSidebarVisibility({}));
  }

  markLayer(id): void {
    console.log(`Hi there - ${id}!`);
  }
}
