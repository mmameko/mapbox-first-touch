export enum MapDrivingModeEnum {
  DRIVING_TRAFFIC = 'driving-traffic',
  DRIVING = 'driving',
  WALKING = 'walking',
  CYCLING = 'cycling'
}
