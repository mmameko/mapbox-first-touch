export enum MapPathColorEnum {
  HARD_TRAFFIC = '#F00',
  MEDIUM_TRAFFIC = '#FFA500',
  LIGHT_TRAFFIC = '#ff0',
  FREE_TRAFFIC = '#5cff5c'
}
