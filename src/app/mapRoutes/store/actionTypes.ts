export enum ActionTypes {
  ROUTES = '[Routes] Get Routes',
  ROUTES_SUCCESS = '[Routes] Get Routes Success',
  ROUTES_FAILURE = '[Routes] Get Routes Failure',

  ADD_POINT = '[Routes] Add Point',
  DELETE_POINT = '[Routes] Delete Point',

  CHANGE_DRIVING_MODE = '[Routes] Change Driving Mode',
  SELECT_ROUTE = '[Routes] Select Alternative Route',

  TOGGLE_SIDEBAR_VISIBILITY = '[Routes] Toggle Sidebar Visibility',

  RESET_ROUTES = '[Routes] Reset Model'
}
