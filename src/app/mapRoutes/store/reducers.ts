import {MapStateInterface} from '../types/mapState.interface';
import {Action, createReducer, on} from '@ngrx/store';
import {
  mapRoutesAction,
  addPoint,
  deletePoint,
  routesSuccessAction,
  routesFailureAction,
  changeDrivingMode,
  selectRouteMode, toggleSidebarVisibility, resetModel
} from './actions/mapRoutesAction';
import {PointInterface} from 'src/app/shared/types/point.interface';
import {MapDrivingModeEnum} from '../enums/mapDrivingMode.enum';

const initialState: MapStateInterface = {
  isSubmitting: false,
  points: null,
  routes: null,
  selectedRouteId: 0,
  error: null,
  drivingMode: MapDrivingModeEnum.DRIVING,
  isSidebarVisible: true
};

const routeReducers = createReducer(
  initialState,
  on(mapRoutesAction, (state): MapStateInterface => ({
    ...state,
    isSubmitting: true,
    routes: null,
    selectedRouteId: 0,
    error: null
  })),
  on(routesSuccessAction, (state: MapStateInterface, { routes }): MapStateInterface => ({
      ...state,
      isSubmitting: false,
      routes: [...routes]
    })),
  on(routesFailureAction, (state: MapStateInterface, { error }): MapStateInterface => ({
      ...state,
      isSubmitting: false,
      error
    })),
  on(addPoint, (state: MapStateInterface, point: PointInterface): MapStateInterface => ({
    ...state,
    points: state.points ? [...state.points, point] : [point]
  })),
  on(deletePoint, (state: MapStateInterface, action) => ({
    ...state,
    points: [
      ...state.points.slice(0, action.id),
      ...state.points.slice(action.id + 1)
    ]
  })),
  on(changeDrivingMode, (state: MapStateInterface, {drivingMode}) => ({
    ...state,
    drivingMode
  })),
  on(selectRouteMode, (state: MapStateInterface, {selectedRouteId}) => ({
    ...state,
    selectedRouteId
  })),
  on(toggleSidebarVisibility, (state: MapStateInterface) => ({
    ...state,
    isSidebarVisible: !state.isSidebarVisible
  })),
  on(resetModel, (state: MapStateInterface, defaultState) => ({
    ...state,
    ...initialState,
    ...defaultState
  }))
);

export function reducers(state: MapStateInterface, action: Action): any {
  return routeReducers(state, action);
}
