import {createFeatureSelector, createSelector} from '@ngrx/store';
import {AppStateInterface} from '../../shared/types/appState.interface';
import {MapStateInterface} from '../types/mapState.interface';
import {MapRouteInterface} from '../types/mapRoute.interface';
import {MapClusteredRouteInterface} from '../types/mapClusteredRoute.interface';

export const routesFeatureSelector = createFeatureSelector<
  AppStateInterface,
  MapStateInterface
  >('routes');

export const isSubmittingSelector = createSelector(
  routesFeatureSelector,
  (routesState: MapStateInterface) => routesState.isSubmitting);

export const pointsSelector = createSelector(
  routesFeatureSelector,
  (routesState: MapStateInterface) => routesState.points);

export const routesSelector = createSelector(
  routesFeatureSelector,
  (routesState: MapStateInterface) => routesState.routes);

export const selectedRouteIdSelector = createSelector(
  routesFeatureSelector,
  (routesState: MapStateInterface) => routesState.selectedRouteId);

export const selectedRouteSelector = createSelector(
  routesFeatureSelector,
  (routesState: MapStateInterface) => {
    return routesState.routes ? routesState.routes[routesState.selectedRouteId] : null;
  });

export const selectedRouteClustersSelector = createSelector(
  routesFeatureSelector,
  (routesState: MapStateInterface) => {
    if (routesState.routes && routesState.routes.length) {
      const selectedRoute: MapRouteInterface = routesState.routes[routesState.selectedRouteId];
      const clusters: MapClusteredRouteInterface = {};

      selectedRoute.paths.forEach((path) => {
        const {weight} = path;

        if (!clusters[weight]) {
          clusters[weight] = [];
        }

        clusters[weight] = [
          ...clusters[weight],
          path
        ];
      });

      return clusters;
    } else {
      return null;
    }
  });

export const unSelectedRoutesSelector = createSelector(
  routesFeatureSelector,
  (routesState: MapStateInterface) => {
    return routesState.routes && routesState.routes
      .filter((route, id) => id !== routesState.selectedRouteId);
  });

export const drivingModeSelector = createSelector(
  routesFeatureSelector,
  (routesState: MapStateInterface) => routesState.drivingMode);

export const isSidebarVisibleSelector = createSelector(
  routesFeatureSelector,
  (routesState: MapStateInterface) => routesState.isSidebarVisible);
