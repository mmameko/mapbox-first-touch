import {createAction, props} from '@ngrx/store';
import {ActionTypes} from '../actionTypes';
import { PointInterface } from 'src/app/shared/types/point.interface';
import {RoutesResponseErrorInterface} from '../../types/mbResponseError.interface';
import {MapRouteInterface} from '../../types/mapRoute.interface';
import {MapStateInterface} from '../../types/mapState.interface';

export const mapRoutesAction = createAction(
  ActionTypes.ROUTES,
  props<{ points: PointInterface[] }>()
);

export const routesSuccessAction = createAction(
  ActionTypes.ROUTES_SUCCESS,
  props<{ routes: MapRouteInterface[] }>()
);

export const routesFailureAction = createAction(
  ActionTypes.ROUTES_FAILURE,
  props<{ error: RoutesResponseErrorInterface }>()
);

export const addPoint = createAction(
  ActionTypes.ADD_POINT,
  props<PointInterface>()
);

export const deletePoint = createAction(
  ActionTypes.DELETE_POINT,
  props<{id: number}>()
);

export const changeDrivingMode = createAction(
  ActionTypes.CHANGE_DRIVING_MODE,
  props<{drivingMode: string}>()
);

export const selectRouteMode = createAction(
  ActionTypes.SELECT_ROUTE,
  props<{selectedRouteId: number}>()
);

export const toggleSidebarVisibility = createAction(
  ActionTypes.TOGGLE_SIDEBAR_VISIBILITY,
  props<{isSidebarVisible?: boolean}>()
);

export const resetModel = createAction(
  ActionTypes.RESET_ROUTES,
  props<{defaultModel?: MapStateInterface}>()
);
