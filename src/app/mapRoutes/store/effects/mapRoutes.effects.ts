import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {mapRoutesAction, routesFailureAction, routesSuccessAction} from '../actions/mapRoutesAction';
import {catchError, debounceTime, filter, map, switchMap, withLatestFrom} from 'rxjs/operators';
import {MbRoutesService} from '../../services/mbRoutes.service';
import {Store} from '@ngrx/store';
import {drivingModeSelector, pointsSelector} from '../selectors';
import {HttpErrorResponse} from '@angular/common/http';
import { of } from 'rxjs/internal/observable/of';

@Injectable()
export class MapRoutesEffects {
  routes$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(mapRoutesAction),
      debounceTime(1000),
      withLatestFrom(
        this.store.select(pointsSelector),
        this.store.select(drivingModeSelector)
      ),
      filter(([action, points, drivingMode]) => points.length > 1),
      switchMap(([action, points, drivingMode]) => {
        return this.routesService.getRoutes(points, drivingMode)
          .pipe(
            map((routes) => {
              return routesSuccessAction({ routes });
            }),
            catchError((errorResponse: HttpErrorResponse) => {
              return of(routesFailureAction({
                error: errorResponse.error
              }));
            })
          );
      }));
  });

  constructor(
    private actions$: Actions,
    private routesService: MbRoutesService,
    private store: Store) {
  }
}
