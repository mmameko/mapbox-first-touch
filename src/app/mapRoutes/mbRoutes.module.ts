import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MapRoutesComponent} from './components/mapRoutes/mapRoutes.component';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {NgxMapboxGLModule} from 'ngx-mapbox-gl';
import {environment} from '../../environments/environment';
import {StoreModule} from '@ngrx/store';
import {reducers} from './store/reducers';
import {HttpClient} from '@angular/common/http';
import {EffectsModule} from '@ngrx/effects';
import {MapRoutesEffects} from './store/effects/mapRoutes.effects';
import {MbRoutesService} from './services/mbRoutes.service';
import {SelectButtonModule} from 'primeng/selectbutton';
import {SidebarModule} from 'primeng/sidebar';
import {ButtonModule} from 'primeng/button';
import {MessageModule} from 'primeng/message';
import {TooltipModule} from 'primeng/tooltip';
import {ListboxModule} from 'primeng/listbox';
import {DurationModule} from '../shared/modules/duration/duration.module';
import {CoordinateModule} from '../shared/modules/coordinate/coordinate.module';
import {RandomizeModule} from '../shared/modules/randomize/randomize.module';

const routes: Routes = [
  {
    path: '',
    component: MapRoutesComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    NgxMapboxGLModule.withConfig({
      accessToken: environment.mapToken
    }),
    StoreModule.forFeature('routes', reducers),
    EffectsModule.forFeature([MapRoutesEffects]),
    MessageModule,
    ButtonModule,
    SidebarModule,
    SelectButtonModule,
    TooltipModule,
    ListboxModule,

    DurationModule,
    CoordinateModule,
    RandomizeModule
  ],
  providers: [
    HttpClient,
    MbRoutesService
  ],
  declarations: [
    MapRoutesComponent
  ]
})
export class MbRoutesModule {}
