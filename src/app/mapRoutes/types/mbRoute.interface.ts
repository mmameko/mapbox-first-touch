import {MbGeometryInterface} from './mbGeometry.interface';

export interface MbRouteInterface {
  distance: number;
  duration: number;
  geometry: MbGeometryInterface;
}
