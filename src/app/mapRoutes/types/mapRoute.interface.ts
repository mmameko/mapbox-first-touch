import {MapPathInterface} from './mapPath.interface';

export interface MapRouteInterface {
  distance: number;
  duration: number;
  paths: MapPathInterface[];
}
