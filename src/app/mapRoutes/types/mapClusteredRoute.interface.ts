import {MapPathInterface} from './mapPath.interface';

export interface MapClusteredRouteInterface {
  [key: string]: MapPathInterface[];
}
