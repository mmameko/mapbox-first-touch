export interface RoutesResponseErrorInterface {
  [key: string]: string[];
}
