import {MbGeometryInterface} from './mbGeometry.interface';
import {MapPathColorEnum} from '../enums/mapPathColor.enum';

export interface MapPathInterface {
  geometry: MbGeometryInterface;
  weight: MapPathColorEnum;
}
