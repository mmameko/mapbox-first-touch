export interface MbGeometryInterface {
  type: string;
  coordinates: Array<[number, number]>;
}
