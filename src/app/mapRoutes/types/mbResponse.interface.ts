import {MbRouteInterface} from './mbRoute.interface';

export interface MbResponseInterface {
  routes: MbRouteInterface[];
}
