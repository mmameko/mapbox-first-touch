import {PointInterface} from 'src/app/shared/types/point.interface';
import {RoutesResponseErrorInterface} from './mbResponseError.interface';
import {MapDrivingModeEnum} from '../enums/mapDrivingMode.enum';
import {MapRouteInterface} from './mapRoute.interface';

export interface MapStateInterface {
  isSubmitting: boolean;
  points: PointInterface[] | null;
  routes: MapRouteInterface[] | null;
  selectedRouteId: number;
  error: RoutesResponseErrorInterface | null;
  drivingMode: MapDrivingModeEnum;
  isSidebarVisible: boolean;
}
