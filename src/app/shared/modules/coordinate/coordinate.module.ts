import {NgModule} from '@angular/core';
import {CoordinatePipe} from './pipes/coordinate.pipe';

@NgModule({
  declarations: [CoordinatePipe],
  exports: [CoordinatePipe]
})
export class CoordinateModule {}
