import {Pipe, PipeTransform} from '@angular/core';
import { PointInterface } from 'src/app/shared/types/point.interface';

@Pipe({
  name: 'coordinate'
})
export class CoordinatePipe implements PipeTransform {
  transform(point: PointInterface, ...args): any {
    let {lat, lng} = point;
    const dim = Math.pow(10, 4);

    lat = Math.round((lat + Number.EPSILON) * dim) / dim;
    lng = Math.round((lng + Number.EPSILON) * dim) / dim;

    return `${this.transformLatitude(lat)}, ${this.transformLongitude(lng)}`;
  }

  private transformLatitude(lat: number): string {
    const absLat = Math.abs(lat);
    let result = `${absLat}${String.fromCharCode(176)} `;

    result = result + (absLat !== lat ? 'S' : 'N');

    return result;
  }

  private transformLongitude(lng: number): string {
    const absLng = Math.abs(lng);
    let result = `${absLng}${String.fromCharCode(176)} `;

    result = result + (absLng !== lng ? 'W' : 'E');

    return result;
  }
}
