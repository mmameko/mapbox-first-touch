import {NgModule} from '@angular/core';
import {DurationPipe} from './pipes/duration.pipe';

@NgModule({
  declarations: [DurationPipe],
  exports: [DurationPipe]
})
export class DurationModule {}
