import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'duration'
})
export class DurationPipe implements PipeTransform {
  transform(seconds: number, ...args): string {
    const hours: number = Math.floor(seconds / 3600);
    const minutes: number = Math.floor((seconds - hours * 3600) / 60);
    const secondsLeft: number = Math.floor(seconds - hours * 3600 - minutes * 60);
    let result = '';

    if (hours) {
      result += `${hours}h `;
    }

    if (minutes) {
      result += `${minutes}m `;
    }

    if (secondsLeft) {
      result += `${secondsLeft}s`;
    }

    return result;
  }
}
