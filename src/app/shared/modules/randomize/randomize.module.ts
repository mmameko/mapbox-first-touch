import {NgModule} from '@angular/core';
import {RandomizeService} from './services/randomize.service';

@NgModule({
  providers: [RandomizeService]
})
export class RandomizeModule {}
