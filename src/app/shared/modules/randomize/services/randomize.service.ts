import {Injectable} from '@angular/core';

@Injectable()
export class RandomizeService {
  randomIntFromRange(min: number, max: number): number {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  randomFloatFromRange(min: number, max: number): number {
    return Math.random() * (max - min) + min;
  }

  randomFromEnum<T>(enumArg: T): T[keyof T] {
    const enumValues = Object.keys(enumArg);
    const id = this.randomIntFromRange(0, enumValues.length - 1);

    return enumArg[enumValues[id]];
  }
}
