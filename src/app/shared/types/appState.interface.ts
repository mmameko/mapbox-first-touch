import {MapStateInterface} from '../../mapRoutes/types/mapState.interface';
import {MapSitesStateInterface} from '../../mapSites/types/mapSitesState.interface';

export interface AppStateInterface {
  routes: MapStateInterface;
  sites: MapSitesStateInterface;
}
