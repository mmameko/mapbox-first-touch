export interface PointInterface {
  lat: number;
  lng: number;
}
