import {createAction, props} from '@ngrx/store';
import { ActionTypes } from '../actionTypes';
import {MapSitesRequestInterface} from '../../types/mapSitesRequest.interface';
import {MapSitesResponseErrorInterface} from '../../types/mapSitesResponseError.interface';
import {MapBoundsInterface} from '../../types/mapBounds.interface';
import {MapSiteResponseInterface} from '../../types/mapSiteResponse.interface';
import {MapSitesStateInterface} from '../../types/mapSitesState.interface';

export const mapSitesAction = createAction(
  ActionTypes.SITES,
  props<{ options?: MapSitesRequestInterface }>()
);

export const mapSitesSuccessAction = createAction(
  ActionTypes.SITES_SUCCESS,
  props<{ sites: MapSiteResponseInterface[] }>()
);

export const mapSitesFailureAction = createAction(
  ActionTypes.SITES_FAILURE,
  props<{ error: MapSitesResponseErrorInterface }>()
);

export const setMapBoundsAction = createAction(
  ActionTypes.SET_MAP_BOUNDS,
  props<{ bounds: MapBoundsInterface }>()
);

export const resetSitesAction = createAction(
  ActionTypes.RESET_SITES,
  props<{ defaultState?: MapSitesStateInterface }>()
);
