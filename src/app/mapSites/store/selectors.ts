import {createFeatureSelector, createSelector} from '@ngrx/store';
import {AppStateInterface} from '../../shared/types/appState.interface';
import {MapSitesStateInterface} from '../types/mapSitesState.interface';

export const sitesFeatureSelector = createFeatureSelector<
    AppStateInterface,
    MapSitesStateInterface
  >('sites');

export const isSubmittingSelector = createSelector(
  sitesFeatureSelector,
  (state: MapSitesStateInterface) => state.isSubmitting);

export const sitesSelector = createSelector(
  sitesFeatureSelector,
  (state: MapSitesStateInterface) => state.sites);

export const mapBoundsSelector = createSelector(
  sitesFeatureSelector,
  (state: MapSitesStateInterface) => state.bounds);
