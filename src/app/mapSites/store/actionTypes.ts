export enum ActionTypes {
  SITES = '[Sites] Get Sites',
  SITES_SUCCESS = '[Sites] Successfully got sites',
  SITES_FAILURE = '[Sites] Failure while getting sites',

  SET_MAP_BOUNDS = '[Sites] Set map bounds',

  RESET_SITES = '[Sites] Clear store'
}
