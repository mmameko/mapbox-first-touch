import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {MapSitesService} from '../../services/mapSites.service';
import {Store} from '@ngrx/store';
import {mapSitesFailureAction, mapSitesSuccessAction, setMapBoundsAction} from '../actions/mapSites.actions';
import {catchError, debounceTime, map, switchMap, withLatestFrom} from 'rxjs/operators';
import {HttpErrorResponse} from '@angular/common/http';
import {of} from 'rxjs';
import {mapBoundsSelector} from '../selectors';

@Injectable()
export class MapSitesEffects {
  sites$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(setMapBoundsAction),
      debounceTime(300),
      withLatestFrom(
        this.store.select(mapBoundsSelector)
      ),
      switchMap(([action, bounds]) => {
        return this.sitesService.getSites(bounds)
          .pipe(
            map((sites) => {
              return mapSitesSuccessAction({sites});
            }),
            catchError((errorResponse: HttpErrorResponse) => {
              return of(mapSitesFailureAction({
                error: errorResponse.error
              }));
            })
          );
      })
    );
  });

  constructor(
    private actions$: Actions,
    private sitesService: MapSitesService,
    private store: Store) {
  }
}
