import {MapSitesStateInterface} from '../types/mapSitesState.interface';
import {Action, createReducer, on} from '@ngrx/store';
import {
  mapSitesAction,
  mapSitesFailureAction,
  mapSitesSuccessAction,
  resetSitesAction,
  setMapBoundsAction
} from './actions/mapSites.actions';

const initialState: MapSitesStateInterface = {
  isSubmitting: false,
  sites: null,
  error: null,
  bounds: null
};

const siteReducers = createReducer(
  initialState,
  on(mapSitesAction, (state): MapSitesStateInterface => ({
    ...state,
    isSubmitting: true,
    error: null,
    sites: null
  })),
  on(mapSitesSuccessAction, (state, { sites }): MapSitesStateInterface => ({
    ...state,
    isSubmitting: false,
    sites: [...sites]
  })),
  on(mapSitesFailureAction, (state, { error }) => ({
    ...state,
    isSubmitting: false,
    error
  })),
  on(setMapBoundsAction, (state, { bounds }) => ({
    ...state,
    bounds
  })),
  on(resetSitesAction, (state, { defaultState }) => ({
    ...state,
    ...initialState,
    ...defaultState
  }))
);

export function reducers(state: MapSitesStateInterface, action: Action): any {
  return siteReducers(state, action);
}
