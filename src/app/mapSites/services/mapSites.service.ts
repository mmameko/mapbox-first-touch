import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {RandomizeService} from 'src/app/shared/modules/randomize/services/randomize.service';
import {take} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {MapBoundsInterface} from '../types/mapBounds.interface';
import {MapSiteResponseInterface} from '../types/mapSiteResponse.interface';

@Injectable()
export class MapSitesService {
  constructor(private randomizeService: RandomizeService) {}

  getSites(bounds: MapBoundsInterface): Observable<MapSiteResponseInterface[]> {
    const sites: MapSiteResponseInterface[] = this.getRandomSites(bounds);

    return of(sites).pipe(
      take(1)
    );
  }

  private getRandomSites(bounds: MapBoundsInterface): MapSiteResponseInterface[] {
    const sites: MapSiteResponseInterface[] = [];
    let { sitesCount } = environment;

    while (sitesCount) {
      sites.push({
        geometry: {
          type: 'Point',
          coordinates: [
            this.randomizeService.randomFloatFromRange(bounds.west, bounds.east),
            this.randomizeService.randomFloatFromRange(bounds.south, bounds.north)
          ]
        },
        properties: {
          id: `${sitesCount}`,
          name: `Site ${sitesCount}`
        }
      });
      sitesCount--;
    }

    return sites;
  }
}
