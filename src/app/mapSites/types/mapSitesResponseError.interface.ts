export interface MapSitesResponseErrorInterface {
  [key: string]: string[];
}
