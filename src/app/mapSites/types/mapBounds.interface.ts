export interface MapBoundsInterface {
  north: number;
  south: number;
  west: number;
  east: number;
}
