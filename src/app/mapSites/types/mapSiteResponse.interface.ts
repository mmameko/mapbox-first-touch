export interface MapSiteResponseInterface {
  geometry: {
    type: string;
    coordinates: [number, number];
  };
  properties: {
    [key: string]: string
  };
}
