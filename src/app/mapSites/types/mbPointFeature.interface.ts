export interface MbPointFeatureInterface {
  type: string;
  properties: {
    [key: string]: string;
  };
  geometry: {
    coordinates: [number, number]
  };
}
