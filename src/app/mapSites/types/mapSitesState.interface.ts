import {MapSitesResponseErrorInterface} from './mapSitesResponseError.interface';
import {MapBoundsInterface} from './mapBounds.interface';
import {MapSiteResponseInterface} from './mapSiteResponse.interface';

export interface MapSitesStateInterface {
  isSubmitting: boolean;
  sites: MapSiteResponseInterface[] | null;
  error: MapSitesResponseErrorInterface | null;
  bounds: MapBoundsInterface | null;
}
