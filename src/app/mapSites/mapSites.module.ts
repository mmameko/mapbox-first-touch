import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MapSitesComponent} from './components/mapSites.component';
import {CommonModule} from '@angular/common';
import {NgxMapboxGLModule} from 'ngx-mapbox-gl';
import {environment} from '../../environments/environment';
import {FormsModule} from '@angular/forms';
import {StoreModule} from '@ngrx/store';
import {reducers} from './store/reducers';
import {MapSitesService} from './services/mapSites.service';
import {EffectsModule} from '@ngrx/effects';
import {MapSitesEffects} from './store/effects/mapSites.effects';
import {RandomizeModule} from '../shared/modules/randomize/randomize.module';
import {TooltipModule} from 'primeng/tooltip';

const routes: Routes = [
  {
    path: '',
    component: MapSitesComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    NgxMapboxGLModule.withConfig({
      accessToken: environment.mapToken
    }),
    StoreModule.forFeature('sites', reducers),
    EffectsModule.forFeature([MapSitesEffects]),
    RandomizeModule,
    TooltipModule
  ],
  providers: [
    MapSitesService
  ],
  declarations: [MapSitesComponent]
})
export class MapSitesModule {}
