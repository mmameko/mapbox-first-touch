import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {environment} from '../../../environments/environment';
import {select, Store} from '@ngrx/store';
import {mapSitesAction, resetSitesAction, setMapBoundsAction} from '../store/actions/mapSites.actions';
import {Map} from 'mapbox-gl';
import {Observable} from 'rxjs';
import {sitesSelector} from '../store/selectors';
import {MapSiteResponseInterface} from '../types/mapSiteResponse.interface';
import {MapBoundsInterface} from '../types/mapBounds.interface';

@Component({
  selector: 'app-sites',
  templateUrl: './mapSites.component.html',
  styleUrls: ['./mapSites.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MapSitesComponent implements OnInit, OnDestroy {
  map: Map;
  mapConfigs = environment.mapConfigs;
  previousBounds: MapBoundsInterface | null = null;

  sites$: Observable<MapSiteResponseInterface[]>;

  constructor(private store: Store) {}

  ngOnInit(): void {
    this.store.dispatch(mapSitesAction({}));
    this.initValues();
  }

  ngOnDestroy(): void {
    this.store.dispatch(resetSitesAction({}));
  }

  initValues(): void {
    this.sites$ = this.store.pipe(
      select(sitesSelector)
    );
  }

  loadMap(map: Map): void {
    this.map = map;
    this.setMapBounds();
  }

  setMapBounds(): void {
    const bf = this.map.getBounds();
    const bounds = {
      north: bf.getNorth(),
      south: bf.getSouth(),
      west: bf.getWest(),
      east: bf.getEast()
    };

    if (!this.previousBounds) {
      this.previousBounds = bounds;
      this.store.dispatch(setMapBoundsAction({ bounds }));
      return;
    }

    if (bounds.north > this.previousBounds.north
      || bounds.south < this.previousBounds.south
      || bounds.east > this.previousBounds.east
      || bounds.west < this.previousBounds.west) {
      this.previousBounds = bounds;
      this.store.dispatch(setMapBoundsAction({ bounds }));
    }
  }
}
