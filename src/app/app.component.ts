import {Component, ViewEncapsulation} from '@angular/core';
import {MenuItem} from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  title = 'mapbox-first-touch';
  menuItems: MenuItem[] = [
    {
      label: 'Routes',
      routerLink: 'routes'
    },
    {
      label: 'Sites',
      routerLink: 'sites'
    }
  ];

  clickMap(e): void {
    console.log(e);
  }
}
