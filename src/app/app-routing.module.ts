import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'routes'
  },
  {
    path: 'routes',
    loadChildren: () => import('./mapRoutes/mbRoutes.module').then(m => m.MbRoutesModule)
  },
  {
    path: 'sites',
    loadChildren: () => import('./mapSites/mapSites.module').then(m => m.MapSitesModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
