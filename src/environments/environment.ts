// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  sitesCount: 1000,
  mapConfigs: {
    style: 'mapbox://styles/mike-mameko/ckh7uk37g059e19lliiu9ytdo',
    zoom: [13],
    center: [-71.0589, 42.360],
    line: {
      layout: {
        'line-join': 'round',
        'line-cap': 'round'
      },
      paint: {
        'line-width': 6
      }
    },
    inactiveLine: {
      layout: {
        'line-join': 'round',
        'line-cap': 'round'
      },
      paint: {
        'line-width': 4,
        'line-opacity': .5,
        'line-color': '#555'
      }
    }
  },
  mapApiUrl: 'https://api.mapbox.com/directions/v5/mapbox/',
  mapToken: 'pk.eyJ1IjoibWlrZS1tYW1la28iLCJhIjoiY2toNTFwc2txMDNkZzJycGIyd2txMjduMSJ9.FSoWJVF5kAbt9O03g9XHoA'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
