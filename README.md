# Mapbox First Touch

## [Demo](https://mmameko.gitlab.io/mapbox-first-touch)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.3.

## Get Started

### Run locally

- Clone the project
- Run `npm install` command
- Run `ng serve` to run dev server
- Navigate to [localhost](http://localhost:4200/)

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Features Review

### Routes Page

The page gives ability to search routes by setting up direction points on the map.
Just click on the map to add a new direction.
Routes will be displayed automatically.
User is able to remove direction points from the sidebar on the left and change the driving mode (driving, walking etc.).
There could be more than 1 possible route to achieve destinations.
So, you will be able to review all of them at the list on the left sidebar.
To select some particular route, please click on the item from the routes list. 

### Sites Page

The page gives ability to review cellular sites on the map.
The main idea was to optimize interacting with a user.
The first step is how we are getting the data: at the moment of time user 
can see only the part of the map (dependent on the scene center and zoom), so, 
there is no sense to get total list of sites. We need only part of them, limited by 
map bounds ([minLat, maxLat], [minLng, maxLng]).
When user interacts with the map (zooming, panning) I just response the next portion of data.
Also rendering process should be optimized. For this purpose I have used clusterization.
So, the violet points with numbers inside are clusters represent the amount of sites in them.
Dark gray points are the separate sites. You can review the name of particular site just hovering on the particular poin on the map.  
